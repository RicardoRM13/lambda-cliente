package com.ricardorm13;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import org.json.JSONArray;


import java.util.Collections;
import java.util.List;


public class LambdaClientes implements RequestHandler<RequestEvent, ApiGatewayResponse> {


    public ApiGatewayResponse handleRequest(RequestEvent requestEvent, Context context) {

        System.out.println("Iniciando lambda");
        ClienteService clienteService = new ClienteService();
        List<Cliente> clientes = clienteService.listaClientes(requestEvent);
        JSONArray json = new JSONArray(clientes);
        return ApiGatewayResponse.builder()
                .setStatusCode(200)
                .setRawBody(json.toString())
                .setHeaders(Collections.singletonMap("X-Powered-By", "Ricardo Ribeiro Marques"))
                .build();
    }
}
