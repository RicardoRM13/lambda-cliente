package com.ricardorm13;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClienteService {

    static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
    static DynamoDBMapper mapper = new DynamoDBMapper(client);
    //private Regions region = Regions.SA_EAST_1;

    public List<Cliente> listaClientes(RequestEvent req){
        DynamoDBScanExpression dbScanExpression = new DynamoDBScanExpression();

        if (getParam(req) != null) {
            Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
            expressionAttributeValues.put(":nome", new AttributeValue().withS(getParam(req)));
            dbScanExpression.withFilterExpression("contains(nome, :nome)");
            dbScanExpression.withExpressionAttributeValues(expressionAttributeValues);
        }
        List<Cliente> lista = mapper.scan(Cliente.class, dbScanExpression);

        return lista;
    }

    private String getParam(RequestEvent req){
        if (req.getQueryStringParameters() != null){
            String json = req.getQueryStringParameters().get("nome").toString();
            System.out.println("Nome: " + json);
            String nome = req.getQueryStringParameters().get("nome").toString();

            return nome;
        }
        return null;
    }
}
