package com.ricardorm13;

import java.util.Map;

public class RequestEvent {
    private Map<String, Object> queryStringParameters;

    public Map<String, Object> getQueryStringParameters() {
        return this.queryStringParameters;
    }

    public void setQueryStringParameters(Map<String, Object> queryParam) {
        this.queryStringParameters = queryParam;
    }

}
