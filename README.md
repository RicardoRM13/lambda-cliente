# lambda-cliente

Essa é uma solução serveless criada para conversão de uma planilha CSV em uma tabela no DynamoDB e com consulta feita através do API Gateway e Lambda Function.
Nesse repositório você encontrará os artefatos abaixo:

1. CloudFormation para criação do DynamoDB, Bucket S3 e um Lambda Function feito em Python.
2. Código fonte de um Lambda Function feito em Java que é executado pelo API Gateway e acessa o DynamoDB.


**Observação: Essa Lambda Function em Java deve ser configurada no API Gateway usando a opção de proxy lambda.**

![Screenshot](serveless-api.png)